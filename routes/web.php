<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/layoutApp/master', function () {
//     return view('layoutApp.master');
// });

//fitur Profile 
Route::get('/profile/{id}','ProfileController@index')->name('profile.index');
Route::get('/profile/{id}/edit','ProfileController@edit')->name('profile.edit');
Route::put('/profile/{id}', 'ProfileController@update')->name('profile.update');

Route::get('profile/data/{id}/edit', 'ProfileController@editData')->name('profile.editData');
Route::put('profile/data/{id}', 'ProfileController@updateData')->name('profile.updateData');
Route::delete('/profile/{id}', 'ProfileController@destroy')->name('profile.destroy');

Route::get('/profile/follow/{id}/{idfollow}', 'ProfileController@follow')->name('profile.follow');
Route::get('/profile/unfollow/{id}/{idfollow}', 'ProfileController@unfollow')->name('profile.unfollow');



//fitur Posting
Route::get('/post/create','PostController@create')->name('post.create');
Route::post('/post','PostController@store')->name('post.store');
Route::get('/post','PostController@index')->name('post.index');

Route::get('/post/like/{idpost}/{id}', 'postController@like')->name('post.like');
Route::get('/post/liked/{idpost}/{id}', 'postController@liked')->name('post.liked');


// fiture commentt
Route::prefix('comments')->group(function (){
    Route::post('/comment/{idpost}/{id}', 'CommentController@store')->name('comment.store');

    Route::get('/like/{idcomment}/{id}', 'CommentController@like')->name('comment.like');
    Route::get('/liked/{idcomment}/{id}', 'CommentController@liked')->name('comment.liked');
});


Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
