@extends('layoutApp.master')

@section('content')
<div class="container mt-5">
    <div class="row">        
        <div class="col-md-12">
            <div class="card">
                <div class="card-header p-2 bg-dark">
                  <a href="/profile/{{Auth::user()->id}}" class="btn btn-secondary shadow-lg rounded"><i class="fas fa-chevron-circle-left"></i></a>
                </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form action="/profile/data/{{ $profileData->id }}" method="POST">
                        @csrf
                        @method('PUT')
                      <div class="card-body">
                        <div class="form-group">
                          <label for="nameFull">Your Name</label>
                          <input type="text" class="form-control" id="nameFull" name="nameFull" value="{{ old('nameFull', $profileData->nameFull) }}" placeholder="nameFulls" >
                          @error('nameFull')
                            <div class="alert alert-danger">{{ $message }}</div>
                          @enderror
                        </div>

                        <div class="form-group">
                          <label for="bio">Bio</label>
                          <input type="text" class="form-control" id="bio" name="bio" value="{{ old('bio', $profileData->bio) }}" placeholder="bio" >
                          @error('bio')
                            <div class="alert alert-danger">{{ $message }}</div>
                          @enderror
                        </div>

                      </div>
                      <!-- /.card-body -->
                      <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                      </div>
                    </form>

              </div>
        </div>
    
         
    </div>
    
</div>
@endsection