@extends('layoutApp.master')

@section('content')
<div class="container mt-5">
    <div class="row">
        <div class="col-md-3">
            <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle" src="{{ asset('/adminlte/dist/img/user4-128x128.jpg')}}" alt="User profile picture">
                </div>
            
                <h3 class="profile-username text-center">{{ Auth::user()->profile->nameFull }}</h3>
            
                <p class="text-muted text-center">{{ Auth::user()->profile->bio }}</p>
            
                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Followers</b> <a class="float-right">1,322</a>
                  </li>
                  <li class="list-group-item">
                    <b>Following</b> <a class="float-right">543</a>
                  </li>
                <a href="/profile/data/{{ $profile->id }}/edit" class="btn btn-secondary btn-block mt-1"><b>Update Profile</b></a>

                </ul>
                
                @if($user == Auth::user())
                
                @else
                <a href="#" class="btn btn-info btn-block"><b>Follow</b></a>
                @endif

              </div>
        </div>
        
        <div class="col-md-9">
            <div class="card">
                <div class="card-header p-2 bg-dark">
                  <a href="/post/create" class="btn btn-secondary shadow-lg rounded"><i class="fas fa-plus"></i></a>
                  
                </div><!-- /.card-header -->
                @forelse ($post  as $key => $post)
                <div class="card-body">
                  
                  <div class="active tab-pane" id="activity">
                    <div class="tab-content">
                      <!-- Post -->
                      <div class="post">
                        <div class="user-block">
                          <img class="img-circle img-bordered-sm" src="{{ asset('/adminlte/dist/img/user1-128x128.jpg')}}" alt="user image">
                          <span class="username">
                            <a href="#">{{ Auth::user()->profile->nameFull }}</a>
                            <a href="/profile/{{ $post->id }}/edit" class="float-right btn-tool btn btn-info btn-sm ml-1"><i class="fas fa-edit"></i></a>
                            <a href="/profile/{{ $post->id}}" class="float-right btn-tool btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i></a> 
                          </span>
                          
                          <span class="description">{{ $post->created_at->diffForHumans()}}</span>
                        </div>
                        <!-- /.post-block -->
                        <p>
                          <img width="200px" src="{{ url('/data_file/'.$post->image) }}" alt="">
                        </p>
                        <p>
                          {{ $post->caption }}
                        </p>
                        
                        <p>
                          <a href="#" class="link-black text-sm"><i class="far fa-thumbs-up mr-1"></i> Like</a>
                          <span class="float-right">
                            <a href="#" class="link-black text-sm">
                              <i class="far fa-comments mr-1"></i> Comments (5)
                            </a>
                          </span>
                        </p>
                        
                        <input class="form-control form-control-2x" type="text" placeholder="Type a Posting">
                      </div>
                      <!-- /.post -->
                    </div>
                    
                  </div>
                  <!-- /.tab-content -->
                </div><!-- /.card-body -->
                @empty
                  <td>
                    <p align="center" class="mt-2 font-weight-bold">No Posts</p>
                  </td>
                @endforelse

              </div>
        </div>
  
    </div>
    
</div>
@endsection

