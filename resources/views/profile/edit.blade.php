@extends('layoutApp.master')

@section('content')
<div class="container mt-5">
    <div class="row">
        <div class="col-md-3">
            <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle" src="{{ asset('/adminlte/dist/img/user4-128x128.jpg')}}" alt="User profile picture">
                </div>
            
                <h3 class="profile-username text-center">{{ Auth::user()->profile->nameFull }}</h3>
            
                <p class="text-muted text-center">{{ Auth::user()->profile->bio }}</p>
            
                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Followers</b> <a class="float-right">1,322</a>
                  </li>
                  <li class="list-group-item">
                    <b>Following</b> <a class="float-right">543</a>
                  </li>
                </ul>
            
                <a href="#" class="btn btn-info btn-block"><b>Follow</b></a>
              </div>
        </div>
        
        <div class="col-md-9">
            <div class="card">
                <div class="card-header p-2 bg-dark">
                  <a href="/profile" class="btn btn-secondary shadow-lg rounded"><i class="fas fa-chevron-circle-left"></i></a>
                </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form action="/profile/{{$profile->id}}" method="POST">
                        @csrf
                        @method('PUT')
                      <div class="card-body">
                        <div class="form-group">
                          <label for="caption">Caption</label>
                          <input type="text" class="form-control" id="caption" name="caption" value="{{ old('caption', $profile->caption) }}" placeholder="Captions" >
                          @error('caption')
                            <div class="alert alert-danger">{{ $message }}</div>
                          @enderror
                        </div>
                      </div>
                      <!-- /.card-body -->
                      <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                      </div>
                    </form>
                  

                
              </div>
        </div>
    
         
    </div>
    
</div>
@endsection