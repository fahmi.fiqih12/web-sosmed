@extends('layoutApp.master')

@section('content')
<div class="container mt-5">
    <div class="row">
        <div class="col-md-3">
            <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle" src="{{ asset('/adminlte/dist/img/user4-128x128.jpg')}}" alt="User profile picture">
                </div>
            
                <h3 class="profile-username text-center">{{ Auth::user()->profile->nameFull }}</h3>
            
                <p class="text-muted text-center">{{ Auth::user()->profile->bio }}</p>
            
                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Followers</b> <a class="float-right">{{count($user->followerid)}}</a>
                  </li>   
                </ul>
                
                <a href="/profile/data/{{ $user->profile->id }}/edit" class="btn btn-secondary btn-block mt-1"><b>Update Profile</b></a>
                
                <a href="/profile/follow/{{Auth::id()}}/{{$user->id}}" id="follow" style="display: none" class="btn btn-info btn-block"><b>Follow</b></a>
                <a href="/profile/unfollow/{{Auth::id()}}/{{$user->id}}" id="unfollow" style="display: none" class="btn btn-info btn-block"><b>UnFollow</b></a>
                
                <script>
                  if({{$user->id != Auth::id() }}){
                    document.getElementById("follow").style.display = "block";
                  }else{

                  }
                </script>

                @foreach ($user->followerid as $follow)
                    <script>
                      if({{$follow->pivot->user_id == Auth::id()}}){
                        document.getElementById("follow").style.display = "none";
                        document.getElementById("unfollow").style.display = "block";
                      }else{

                      }
                    </script>
                @endforeach

              </div>
        </div>
        
        <div class="col-md-9">
            <div class="card">
                <div class="card-header p-2 bg-dark">
                  <a href="/post/create" class="btn btn-secondary shadow-lg rounded"><i class="fas fa-plus"></i></a>
                  
                </div><!-- /.card-header -->
                @forelse ($post  as $key => $post)
                <div class="card-body">
                  
                  <div class="active tab-pane" id="activity">
                    <div class="tab-content">

                      <!-- Post -->
                      <div class="post">
                        <div class="user-block">
                          <img class="img-circle img-bordered-sm" src="{{ asset('/adminlte/dist/img/user1-128x128.jpg')}}" alt="user image">
                          <span class="username">
                            <a href="#">{{ Auth::user()->profile->nameFull }}</a>
                            <a href="/profile/{{ $post->id }}/edit" class="float-right btn-tool btn btn-info btn-sm ml-1"><i class="fas fa-edit"></i></a>
                            <a href="/profile/{{ $post->id}}" class="float-right btn-tool btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i></a> 
                          </span>
                          
                          <span class="description">{{ $post->created_at->diffForHumans()}}</span>
                        </div>
                        <!-- /.user-block -->
                        <p>
                          <img width="200px" src="{{ url('/data_file/'.$post->image) }}" alt="">
                        </p>
                        <p>
                          {{ $post->caption }}
                        </p>
                        
                        <p>
                          <a href="#" class="link-black text-sm"><i class="far fa-thumbs-up mr-1"></i> Like</a>
                          <span class="float-right">
                            <a href="#" class="link-black text-sm">
                              <i class="far fa-comments mr-1"></i> Comments ({{count($post->comments)}})
                            </a>
                          </span>
                        </p>
                        @foreach ($post->comments as $comment)
                        <p>
                          <a href="/profile/{{$post->user->id}}">{{$post->user->name}}</a> :  {{$comment->comment}}<br>
                          
                          <a href="/comments/like/{{$comment->id}}/{{Auth::id()}}" id="likecommentbtn{{$comment->id}}" class="link-black text-sm"><i class="far fa-thumbs-up mr-1"></i>Like ({{count($comment->commentlikeid)}})</a>
                          <a href="/comments/liked/{{$comment->id}}/{{Auth::id()}}" id="likedcommentbtn{{$comment->id}}" style="display: none" class="link-black text-sm"><i class="far fa-thumbs-up mr-1"></i>Liked ({{count($comment->commentlikeid)}}) </a>
                              </p>
                        @endforeach
          
                        @foreach ($comment->commentlikeid as $like)
                              <script>
                                if({{$like->pivot->user_id == Auth::id()}}){
                                  document.getElementById('likecommentbtn{{$comment->id}}').style.display = "none";
                                  document.getElementById('likedcommentbtn{{$comment->id}}').style.display = "block";
                                }
                              </script>
                        @endforeach
                              
                        <form action="/comments/comment/{{ $post->id }}/{{ Auth::id() }}" method="post">
                          @csrf
                          <div class="input-group input-group-sm mb-0">
                            <input class="form-control form-control-sm" placeholder="Respone" name="comment">
                            <div class="input-group-append">
                              <button type="submit" class="btn btn-info">Send</button>
                            </div>
                          </div>
                        </form>
                      </div>
                      <!-- /.post -->
                    </div>
                    
                  </div>
                  <!-- /.tab-content -->
                </div><!-- /.card-body -->
                @empty
                  <td>
                    <p align="center" class="mt-2 font-weight-bold">No Posts</p>
                  </td>
                @endforelse

              </div>
        </div>
  
    </div>
    
</div>
@endsection

