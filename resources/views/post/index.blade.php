@extends('layoutApp.master')

@section('content')
    
  <div class="container">
    <div class="card">
      <div class="card-header p-2 bg-dark">
        {{-- <ul class="nav nav-pills">
          <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab"></a></li>
        </ul> --}}
        <a href="{{ url('/post/create')}}" class="btn btn-dark shadow-lg rounded"><i class="fas fa-plus"> Add</i></a>
        
      </div><!-- /.card-header -->
      @forelse ($post as $post)
      <div class="card-body">
        <div class="tab-content">
          <div class="active tab-pane" id="activity">
            
            <!-- Post -->
            <div class="post">
              <div class="user-block">
                <img class="img-circle img-bordered-sm" src="{{ asset('/adminlte/dist/img/user1-128x128.jpg')}}" alt="user image">
                <span class="username">
                  <a href="/profile/{{ $post->user->id }}">{{ $post->user->name }}</a>
                </span>
                <span class="description">{{ $post->created_at->diffForHumans()}}</span>
              </div>
              <!-- /.user-block -->
              <p>
                <img width="200px" src="{{ url('/data_file/'.$post->image) }}" alt="">
              </p>
              <p>
                {{ $post->caption }}
              </p>

              <p>
                <a href="/post/like/{{$post->id}}/{{Auth::id()}}" id="likebtn{{$post->id}}" class="link-black text-sm"><i class="far fa-thumbs-up mr-1"></i>Like ({{count($post->postlikeid)}})</a>
                <a href="/post/liked/{{$post->id}}/{{Auth::id()}}" style="display: none" id="likedbtn{{$post->id}}" class="link-black text-sm"><i class="far fa-thumbs-up mr-1"> Liked ({{count($post->postlikeid)}})</i></a>
                
                                
                @foreach($post->postlikeid as $h)
                    <script>
                      if({{$h->pivot->user_id == Auth::id()}}){
                        document.getElementById("likebtn{{$post->id}}").style.display = "none";
                        document.getElementById("likedbtn{{$post->id}}").style.display = "block";
                      }else{

                      }
                    </script>
                @endforeach


                <span class="float-right">
                  <a href="#" class="link-black text-sm">
                    <i class="far fa-comments mr-1 mb-2"></i> Comments ({{count($post->comments)}})
                  </a>
                </span>
              </p>
              
              @foreach ($post->comments as $comment)
                    <p>
                      <a href="/profile/{{$post->user->id}}">{{$post->user->name}}</a> :  {{$comment->comment}}<br>
                      
                      <a href="/comments/like/{{$comment->id}}/{{Auth::id()}}" id="likecommentbtn{{$comment->id}}" class="link-black text-sm"><i class="far fa-thumbs-up mr-1"></i>Like ({{count($comment->commentlikeid)}})</a>
                      <a href="/comments/liked/{{$comment->id}}/{{Auth::id()}}" id="likedcommentbtn{{$comment->id}}" style="display: none" class="link-black text-sm"><i class="far fa-thumbs-up mr-1"></i>Liked ({{count($comment->commentlikeid)}}) </a>
                    </p>
              @endforeach

              @foreach ($comment->commentlikeid as $like)
                    <script>
                      if({{$like->pivot->user_id == Auth::id()}}){
                        document.getElementById('likecommentbtn{{$comment->id}}').style.display = "none";
                        document.getElementById('likedcommentbtn{{$comment->id}}').style.display = "block";
                      }
                    </script>
              @endforeach

              <form action="/comments/comment/{{ $post->id }}/{{ Auth::id() }}" method="post">
                @csrf
                <div class="input-group input-group-sm mb-0">
                  <input class="form-control form-control-sm" placeholder="Respone" name="comment">
                  <div class="input-group-append">
                    <button type="submit" class="btn btn-info">Send</button>
                  </div>
                </div>
              </form>
            </div>
           
            
            
            
            <!-- /.post -->
          </div>

        </div>
        <!-- /.tab-content -->
      </div><!-- /.card-body -->
      @empty
      <p align="center">No Posting</p>
      @endforelse
    </div>
  </div>
@endsection