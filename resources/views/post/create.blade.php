@extends('layoutApp.master')

@section('content')
    <div class="container">

        <div class="card card-dark">
            <div class="card-header">
            <h3 class="card-title">Create Posting</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="/post" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="form-group">
                <label for="caption">Caption</label>
                <input type="text" class="form-control" name="caption" id="caption" placeholder="Enter Posting">
                </div>
                <div class="form-group">
                <label for="file">File input</label>
                <div class="input-group">
                    <div class="custom-file">
                    <input type="file" name="image" class="btn btn-dark">
                    </div>
                    
                </div>
                </div>
            </div>
            <!-- /.card-body -->
    
            <div class="card-footer">
                <button type="submit" class="btn btn-info">Submit</button>
            </div>
            </form>
        </div>
    </div>
@endsection