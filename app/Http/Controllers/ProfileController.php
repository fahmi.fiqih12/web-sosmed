<?php

namespace App\Http\Controllers;

use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Http\Request;
use App\Profile_follower;
use App\Profile;
use App\Post;
use App\User;
use Auth;


class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $user = User::find($id);
        $post = Post::all()->where('user_id', $id);
        // dd($post->comments);
        return view('profile.index', ['post' => $post], ['user' => $user]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        $post = $user->posts;
        $profile = Porfile::where('id', $id)->first();
        
        return view('profile.show', ['post' => $post], ['user' => $user], ['profile' => $profile]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profile = Post::where('id', $id)->first();
        return view('profile.edit', compact('profile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'caption' => 'required',
        ]);

        $profile = Post::where('id', $id)
        ->update([
            'caption' => $request['caption'],
        ]);
        
        Alert::success('Success Update', 'Success Update profile');
        return redirect('/post');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $post->delete();

        Alert::success('Success Delete', 'File Terhapus');
        return redirect('/post');
    }

    public function editData($id){
        $profileData = Profile::where('id', $id)->first();
        return view('profile.editData', compact('profileData'));
    }

    public function updateData($id, Request $request){
        $request->validate([
            'nameFull' => 'required',
            'bio' => 'required',
        ]);

        $updateData = Profile::where('id', $id)
        ->update([
            'nameFull' => $request['nameFull'],
            'bio' => $request['bio'],
        ]);

        Alert::success('Success Update', 'Success Update profile');
        return redirect('/post');
    }

    public function follow($id, $idfollow){
        $follow = new Profile_follower;
        $follow->user_id = $id;
        $follow->user_follower_id = $idfollow;
        $follow->save();

        return redirect('/post');
    }

    public function unfollow($id, $idfollow){
        $unfollow = Profile_follower::all()->where('user_id', $id)->where('user_follower_id', $idfollow)->first();
        $unfollow->delete();
        return redirect('/post');

    }
}