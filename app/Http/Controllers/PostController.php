<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post_Like;
use App\Post;
use App\User;
use App\Comment;
use Auth;


class PostController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        // $user = Auth::id();
        // $post = $user->posts;
        $post = Post::all();
        // $postDetail = Post::all()->where('user_id', $id);
        $comment = new Comment;
        
        return view('post.index', ['post' => $post], ['comment' => $comment]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // dd($request->all());

        $request->validate([
            'caption' => 'required|max:255',
            'file' => 'file|image|mimes:jpeg.png,jpg|max:2048'           
        ]);
            
        $image = $request->file('image');
        $name_file = time()."_".$image->getClientOriginalName();
        $to = 'data_file';
        $image->move($to ,$name_file);


        $post = Post::create([
            "caption" => $request['caption'],
            "image" => $name_file,
            'user_id' => Auth::id()
        ]);   

        return redirect('/post');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function like($idpost, $id){
        $postlike = new Post_Like;
        
        $postlike->post_id = $idpost;
        $postlike->user_id = $id;

        $postlike->save();
        return redirect('/post');
    }

    
    public function liked($idpost, $id){
        $postlike = Post_Like::where('post_id', $idpost)->where('user_id', $id)->first();
        $postlike->delete();
        return redirect('/post');

    }
}
