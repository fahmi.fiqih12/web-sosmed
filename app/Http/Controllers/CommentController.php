<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\Comment_like;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($idpost, $id, Request $request)
    {
        $comment = new Comment;
        $comment->post_id = $idpost;
        $comment->user_id = $id;
        $comment->comment = $request->comment;
        $comment->save();
    
        return redirect('/post');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function like($idcomment, $id){
        $commentlike = new Comment_like;
        $commentlike->comment_id = $idcomment;
        $commentlike->user_id = $id;
        $commentlike->save();
        return redirect('/post');
    }

    public function liked($idcomment, $id){
        $commentliked = Comment_like::where('comment_id', $idcomment)->where('user_id', $id)->first();
        $commentliked->delete();
        return redirect('/post');
    }

}
