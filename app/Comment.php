<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    
    public function post(){
        $this->belongsTo('App\Post', 'post_id');
    }

    public function user(){
        $this->belongsTo('App\User', 'user_id');
    }

    public function commentlikeid(){
      return $this->belongsToMany('App\User', 'comment_likes', 'comment_id', 'user_id');
    }
    
}
