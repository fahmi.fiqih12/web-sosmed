<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $guarded = [];

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }

    public function postlikeid(){
        return $this->belongsToMany('App\User', 'post_likes', 'post_id', 'user_id');
    }
    
    public function comments(){
        return $this->hasMany('App\Comment', 'post_id');
    }
}
