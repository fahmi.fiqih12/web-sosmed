<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function profile(){
        return $this->hasOne('App\Profile');
    }

    public function posts(){
        return $this->hasMany('App\Post','user_id');
    }
    
    public function postlikeid(){
        return $this->belongsToMany('App\Post', 'post_likes', 'user_id', 'post_id');
    }

    public function comments(){
        return $this->hasMany('App\Comment', 'user_id');
    }

    public function commentlikeid(){
        return $this->belongsToMany('App\comment', 'comment_likes', 'comment_id', 'user_id');
    }

    public function followerid(){
        return $this->belongsToMany('App\User', 'profile_followers', 'user_id', 'user_follower_id');
    }


    
}
